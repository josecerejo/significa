module.exports = {
  bracketSpacing: true,
  printWidth: 120,
  requirePragma: false,
  semi: true,
  singleQuote: true,
  tabWidth: 2,
  usaTabs: false,
  arrowParents: 'avoid',
};
