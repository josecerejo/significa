module.exports = {
  displayName: 'signature tests',
  moduleDirectories: ['node_modules', 'src'],
  setupFilesAfterEnv: ['./src/setupTests.ts'],
  coveragePathIgnorePatterns: ["./src/test-utils", "./src/types"],
  testEnvironment: 'jsdom',
  transform: {
    "^.+\\.tsx?$": "ts-jest",
  },
  moduleNameMapper: {
    "\\.(s?css|sass)$": "identity-obj-proxy"
  },
};
