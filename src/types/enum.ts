export enum HttpRequestStatus {
  UNINITIALIZED,
  PENDING,
  NOT_FOUND,
  FULFILLED,
  REJECTED,
}
