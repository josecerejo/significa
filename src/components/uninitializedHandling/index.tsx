import { FC } from 'react';
import styles from './uninitializedHandling.module.scss';

const UninitializedState: FC = () => {
  return (
    <h1 className={styles.title} data-testid="uninitializedState-message">
      You should search for a movie first
    </h1>
  );
};

export default UninitializedState;
