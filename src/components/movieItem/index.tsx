import { FC, memo } from 'react';
import styles from './movieItem.module.scss';
import { Movie } from '../../types';
import FavoriteIcon from '../favoriteIcon';
import { Link } from 'react-router-dom';

type MovieItemProps = {
  movie: Movie;
};

const MovieItem: FC<MovieItemProps> = memo(({ movie }) => {
  return (
    <article className={styles['movie-item']}>
      <Link to={`/movie/${movie.imdbID}`}>
        <img src={movie.Poster === 'N/A' ? './src/assets/styles/images/gray-background.svg' : movie.Poster} />
      </Link>
      <div className={styles['movie-information']}>
        <div className={styles['year-type-information']}>
          <span>{movie.Year}</span>
          <i className={styles['movie-icon']}></i>
          <span>{movie.Type}</span>
        </div>
        <FavoriteIcon movie={movie} />
      </div>
      <span>{movie.Title}</span>
    </article>
  );
});

export default MovieItem;
