import { FC } from 'react';
import styles from './notFoundHandling.module.scss';

type NotFoundHandlingProps = {
  message?: string;
};

const NotFoundHandling: FC<NotFoundHandlingProps> = ({ message }) => {
  return (
    <section className={styles.container} data-testid="not-found-handling-container">
      <div className={styles['movie-not-found']}></div>
      <h1>{message ? message : 'Movie not found'}</h1>
    </section>
  );
};

export default NotFoundHandling;
