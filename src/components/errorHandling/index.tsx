import { FC } from 'react';
import styles from './errorHandling.module.scss';

const ErrorHandling: FC = () => {
  return (
    <section className={styles.container} data-testid="error-handling-container">
      <div className={styles['error-image']}></div>
      <h1>There was an error processing the request</h1>
    </section>
  );
};

export default ErrorHandling;
