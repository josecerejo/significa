import { FC } from 'react';
import styles from './loadingSpinner.module.scss';

const LoadingSpinner: FC = () => {
  return (
    <div className={styles.container} data-testid="loading-spinner-container">
      <div className={styles.loading}></div>
    </div>
  );
};

export default LoadingSpinner;
