import { FC } from 'react';
import { Link } from 'react-router-dom';
import style from './dropdownListItem.module.scss';

type DropdownListItemProps = {
  title: string;
  hrefLink: string;
};

const DropdownListItem: FC<DropdownListItemProps> = ({ title, hrefLink }) => {
  return (
    <li className={style['list-item']}>
      <Link to={hrefLink}>{title}</Link>
    </li>
  );
};

export default DropdownListItem;
