import React from 'react';
import { render, screen, waitFor } from '../../../test-utils';
import Home from '..';
import { useMoviesListContext } from '../../../context/MoviesListContext';
import { HttpRequestStatus } from '../../../types';

jest.mock('../../../context/MoviesListContext');

const renderComponent = () => render(<Home />);

describe('Home page', () => {
  beforeEach(() => jest.clearAllMocks());

  test('renders UninitializedState when status is UNINITIALIZED', () => {
    (useMoviesListContext as jest.Mock).mockReturnValue({
      httpRequestStatusMovieList: HttpRequestStatus.UNINITIALIZED,
    });

    renderComponent();

    const message = screen.getByTestId('uninitializedState-message');

    expect(message).toBeInTheDocument();
  });

  test('renders LoadingSpinner when status is PENDING', () => {
    (useMoviesListContext as jest.Mock).mockReturnValue({
      httpRequestStatusMovieList: HttpRequestStatus.PENDING,
    });

    renderComponent();

    const loadingSpinner = screen.getByTestId('loading-spinner-container');

    expect(loadingSpinner).toBeInTheDocument();
  });

  test('renders NotFoundHandling when status is NOT_FOUND', () => {
    (useMoviesListContext as jest.Mock).mockReturnValue({
      httpRequestStatusMovieList: HttpRequestStatus.NOT_FOUND,
    });

    renderComponent();

    const notFound = screen.getByTestId('not-found-handling-container');

    expect(notFound).toBeInTheDocument();
  });

  test('renders Movies when status is FULFILLED', async () => {
    (useMoviesListContext as jest.Mock).mockReturnValue({
      httpRequestStatusMovieList: HttpRequestStatus.FULFILLED,
      movies: [],
      movieSearched: 'mockMovieSearched',
      hasMoreMovies: false,
      fetchMovies: jest.fn(),
    });

    renderComponent();

    await waitFor(() => {
      const movies = screen.getByTestId('movies-searched');

      expect(movies).toBeInTheDocument();
    });
  });

  test('renders ErrorHandling when status is REJECTED', () => {
    (useMoviesListContext as jest.Mock).mockReturnValue({
      httpRequestStatusMovieList: HttpRequestStatus.REJECTED,
    });

    renderComponent();

    const errorStatus = screen.getByTestId('error-handling-container');

    expect(errorStatus).toBeInTheDocument();
  });
});
