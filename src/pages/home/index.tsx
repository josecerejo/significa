import { FC } from 'react';
import Movies from '../../features/movies';
import { useMoviesListContext } from '../../context/MoviesListContext';
import { HttpRequestStatus } from '../../types';
import UninitializedState from '../../components/uninitializedHandling';
import LoadingSpinner from '../../components/loadingSpinner';
import ErrorHandling from '../../components/errorHandling';
import NotFoundHandling from '../../components/notFoundHandling';

const Home: FC = () => {
  const { httpRequestStatusMovieList } = useMoviesListContext();

  return (
    <>
      {httpRequestStatusMovieList === HttpRequestStatus.UNINITIALIZED && <UninitializedState />}
      {httpRequestStatusMovieList === HttpRequestStatus.PENDING && <LoadingSpinner />}
      {httpRequestStatusMovieList === HttpRequestStatus.NOT_FOUND && <NotFoundHandling />}
      {httpRequestStatusMovieList === HttpRequestStatus.FULFILLED && <Movies />}
      {httpRequestStatusMovieList === HttpRequestStatus.REJECTED && <ErrorHandling />}
    </>
  );
};

export default Home;
