import { FC } from 'react';
import { useParams } from 'react-router-dom';
import LoadingSpinner from '../../components/loadingSpinner';
import useHttpRequestHandler from '../../hooks/useHttpRequestHandler';
import ErrorHandling from '../../components/errorHandling';
import { MovieDetail } from '../../types';
import MovieDetails from '../../features/movieDetails';

const MovieDetailPage: FC = () => {
  const { id } = useParams();

  const { data: movie, loading, error } = useHttpRequestHandler<MovieDetail>(`i=${id}`);

  return (
    <>
      {loading && <LoadingSpinner />}
      {error && <ErrorHandling />}
      {movie && <MovieDetails movieDetail={movie} />}
    </>
  );
};

export default MovieDetailPage;
