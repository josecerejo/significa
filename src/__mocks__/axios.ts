const axios: any = {
  create: jest.fn(() => axios),
  get: jest.fn(),
};

export default axios;
