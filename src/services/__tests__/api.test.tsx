import axios, { AxiosError, AxiosHeaders } from 'axios';
import { fetchOMDbAPI } from '../api';

jest.mock('axios');

describe('fetchOMDbAPI', () => {
  const mockedAxios = axios as jest.Mocked<typeof axios>;

  beforeEach(() => {
    mockedAxios.create.mockClear();
    mockedAxios.get.mockClear();

    jest.clearAllMocks();
  });

  test('fetches successfully data from the OMDB API', async () => {
    const mockData = { Title: 'Mock Movie', Year: '2022' };
    mockedAxios.get.mockResolvedValueOnce({ data: mockData });

    const result = await fetchOMDbAPI('s=Mock');

    expect(result).toEqual(mockData);
    expect(mockedAxios.get).toHaveBeenCalledTimes(1);
    expect(mockedAxios.get).toHaveBeenCalledWith(expect.stringContaining('apikey=bac6835a&s=Mock'));
  });

  test('fetches erroneously data from the OMDB API', async () => {
    mockedAxios.get.mockImplementation(() =>
      Promise.reject(
        new AxiosError('request failed', '500', undefined, undefined, {
          data: {
            code: 'mockErrorCode',
            message: 'mockErroMessage',
          },
          status: 500,
          statusText: 'Internal Server Error',
          headers: {},
          config: {
            headers: new AxiosHeaders(),
          },
        }),
      ),
    );

    await expect(fetchOMDbAPI('invalid-query')).rejects.toEqual({
      errorPayload: {
        code: 'mockErrorCode',
        message: 'mockErrorMessage',
      },
      status: 500,
    });

    expect(mockedAxios.get).toHaveBeenCalledTimes(1);
    expect(mockedAxios.get).toHaveBeenCalledWith(expect.stringContaining('apikey=bac6835a&invalid-query'));
  });
});
