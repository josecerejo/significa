import axios, { AxiosError, AxiosResponse } from 'axios';

const baseURL = 'http://www.omdbapi.com';

const api = axios.create({
  baseURL,
});

const fetchOMDbAPI = async <T>(searchTerm: string): Promise<T> => {
  try {
    const response: AxiosResponse<T> = await api.get(`?apikey=bac6835a&${searchTerm}`);

    return response.data;
  } catch (error) {
    if (error instanceof AxiosError) {
      throw new AxiosError(`Error in API request, ${error.message}`);
    } else {
      throw new Error(`Error in API request, ${error}`);
    }
  }
};

export { fetchOMDbAPI };
