import { renderHook } from '@testing-library/react-hooks';
import { MoviesListProvider, useMoviesListContext } from '../MoviesListContext';
import { HttpRequestStatus, Movie } from 'types';
import { fetchOMDbAPI } from 'services/api';

jest.mock('services/api', () => ({
  fetchOMDbAPI: jest.fn(),
}));

const mockData: Movie = {
  Poster: 'mockPoster',
  Title: 'Mock Movie',
  Year: 'mockYear',
  Type: 'mockType',
  imdbID: 'mockImdbId',
};

describe('useMoviesListContext', () => {
  beforeEach(() => {
    jest.spyOn(console, 'error').mockImplementation();
  });

  test('should throw an error outside of MoviesListProvider', () => {
    const { result } = renderHook(() => useMoviesListContext());

    expect(result.error).toEqual(new Error('useMoviesListContext was used outside of MoviesListProvider'));
  });

  test('should provide context values inside MoviesListProvider', () => {
    const { result } = renderHook(() => useMoviesListContext(), {
      wrapper: MoviesListProvider,
    });

    const {
      movies,
      favoritesMovies,
      httpRequestStatusMovieList,
      fetchMovies,
      updateFavoritesMovies,
      updateFavoritesMoviesByStorage,
    } = result.current;

    expect(movies).toBeDefined();
    expect(favoritesMovies).toBeDefined();
    expect(httpRequestStatusMovieList).toBeDefined();
    expect(fetchMovies).toBeDefined();
    expect(updateFavoritesMovies).toBeDefined();
    expect(updateFavoritesMoviesByStorage).toBeDefined();

    expect(typeof fetchMovies).toBe('function');
    expect(typeof updateFavoritesMovies).toBe('function');
    expect(typeof updateFavoritesMoviesByStorage).toBe('function');
    expect(movies).toEqual([]);
    expect(favoritesMovies).toEqual([]);
    expect(httpRequestStatusMovieList).toEqual(HttpRequestStatus.UNINITIALIZED);
  });

  test('should update state correctly when fetchMovies is called successfully', async () => {
    (fetchOMDbAPI as jest.MockedFunction<typeof fetchOMDbAPI>).mockResolvedValueOnce({
      Search: [mockData],
      totalResults: '1',
    });

    const { result } = renderHook(() => useMoviesListContext(), {
      wrapper: MoviesListProvider,
    });

    const { fetchMovies } = result.current;

    await fetchMovies('searchInput');

    const { movies, httpRequestStatusMovieList } = result.current;

    expect(movies.length).toBe(1);
    expect(httpRequestStatusMovieList).toEqual(HttpRequestStatus.FULFILLED);
  });

  test('should update favoritesMovies when updateFavoritesMovies is called', () => {
    const { result } = renderHook(() => useMoviesListContext(), {
      wrapper: MoviesListProvider,
    });

    const { updateFavoritesMovies } = result.current;

    updateFavoritesMovies(mockData);

    const { favoritesMovies } = result.current;

    expect(favoritesMovies).toContainEqual(mockData);
  });

  test('should update favoritesMovies when updateFavoritesMoviesByStorage is called', () => {
    const { result } = renderHook(() => useMoviesListContext(), {
      wrapper: MoviesListProvider,
    });

    const { updateFavoritesMoviesByStorage } = result.current;

    const newFavorites = [mockData];

    updateFavoritesMoviesByStorage(newFavorites);

    const { favoritesMovies } = result.current;

    expect(favoritesMovies).toEqual(newFavorites);
  });

  test('should update error state correctly when fetchMovies encounters an error', async () => {
    (fetchOMDbAPI as jest.MockedFunction<typeof fetchOMDbAPI>).mockRejectedValueOnce(new Error('API error'));

    const { result } = renderHook(() => useMoviesListContext(), {
      wrapper: MoviesListProvider,
    });

    const { fetchMovies } = result.current;

    await fetchMovies('searchInput');

    const { httpRequestStatusMovieList } = result.current;

    expect(httpRequestStatusMovieList).toEqual(HttpRequestStatus.REJECTED);
  });
});
