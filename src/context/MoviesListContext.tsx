import { FC, createContext, ReactNode, useContext, useState } from 'react';
import { Movie, HttpRequestStatus } from '../types';
import { fetchOMDbAPI } from '../services/api';

type FetchAPI = {
  Response: string;
  Search: Movie[];
  totalResults: string;
  Error?: string;
};

type MoviesListProviderProps = {
  children: ReactNode;
};

type MovieListContextType = {
  movies: Movie[];
  favoritesMovies: Movie[];
  fetchMovies: (searchInput: string, page?: number) => void;
  updateFavoritesMovies: (movie: Movie) => void;
  updateFavoritesMoviesByStorage: (movies: Movie[]) => void;
  httpRequestStatusMovieList: HttpRequestStatus;
  movieSearched: string;
  hasMoreMovies: boolean;
};

type InternalContextState = {
  movies: Movie[];
  favoritesMovies: Movie[];
  httpRequestStatusMovieList: HttpRequestStatus;
  movieSearched: string;
  totalResults: number;
};

const initialInternalContextState: InternalContextState = {
  movies: [],
  favoritesMovies: [],
  httpRequestStatusMovieList: HttpRequestStatus.UNINITIALIZED,
  movieSearched: '',
  totalResults: 0,
};

export const MoviesListContext = createContext<MovieListContextType | undefined>(undefined);

export const MoviesListProvider: FC<MoviesListProviderProps> = ({ children }) => {
  const [internalContextState, setInternalContextState] = useState<InternalContextState>(initialInternalContextState);

  const fetchMovies = async (searchInput: string, page = 1) => {
    try {
      setInternalContextState({
        ...internalContextState,
        httpRequestStatusMovieList:
          internalContextState.movies.length && searchInput === internalContextState.movieSearched
            ? HttpRequestStatus.FULFILLED
            : HttpRequestStatus.PENDING,
      });

      const movies = await fetchOMDbAPI<FetchAPI>(`s=${searchInput}&type=movie&page=${page}`);

      if (movies?.Search) {
        setInternalContextState({
          ...internalContextState,
          movies:
            searchInput !== internalContextState.movieSearched
              ? movies.Search
              : internalContextState.movies.concat(movies.Search),
          httpRequestStatusMovieList: HttpRequestStatus.FULFILLED,
          movieSearched: searchInput,
          totalResults: Number(movies.totalResults),
        });
      }

      if (movies?.Error) {
        setInternalContextState({
          ...internalContextState,
          movieSearched: searchInput,
          httpRequestStatusMovieList: HttpRequestStatus.NOT_FOUND,
        });
      }
    } catch (error) {
      setInternalContextState({
        ...internalContextState,
        httpRequestStatusMovieList: HttpRequestStatus.REJECTED,
      });
    }
  };

  const updateFavoritesMovies = (movie: Movie) => {
    const isMovieInFavorites = internalContextState.favoritesMovies.some(
      (favoriteMovie) => favoriteMovie.imdbID === movie.imdbID,
    );

    const movies = isMovieInFavorites
      ? internalContextState.favoritesMovies.filter((favoriteMovie) => favoriteMovie.imdbID !== movie.imdbID)
      : internalContextState.favoritesMovies.concat(movie);

    setInternalContextState({
      ...internalContextState,
      favoritesMovies: movies,
    });

    localStorage.setItem('favoriteMovies', JSON.stringify(movies));
  };

  const updateFavoritesMoviesByStorage = (movies: Movie[]) => {
    setInternalContextState({
      ...internalContextState,
      favoritesMovies: movies,
    });
  };

  return (
    <MoviesListContext.Provider
      value={{
        movies: internalContextState.movies,
        favoritesMovies: internalContextState.favoritesMovies,
        httpRequestStatusMovieList: internalContextState.httpRequestStatusMovieList,
        movieSearched: internalContextState.movieSearched,
        hasMoreMovies: internalContextState.movies.length < internalContextState.totalResults,
        fetchMovies: fetchMovies,
        updateFavoritesMovies: updateFavoritesMovies,
        updateFavoritesMoviesByStorage: updateFavoritesMoviesByStorage,
      }}
    >
      {children}
    </MoviesListContext.Provider>
  );
};

export const useMoviesListContext = () => {
  const context = useContext<MovieListContextType | undefined>(MoviesListContext);

  if (!context) {
    throw new Error('useMoviesListContext was used outside of MoviesListProvider');
  }

  return context;
};
