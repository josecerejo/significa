import React, { ReactNode, FC } from 'react';
import { render, RenderOptions, RenderResult } from '@testing-library/react';
import { MoviesListProvider } from 'context/MoviesListContext';
import { MemoryRouter } from 'react-router-dom';

interface AllTheProvidersProps {
  children: ReactNode;
}

const AllTheProviders: FC<AllTheProvidersProps> = ({ children }) => {
  return (
    <MemoryRouter>
      <MoviesListProvider>{children}</MoviesListProvider>
    </MemoryRouter>
  );
};

interface CustomRenderOptions extends RenderOptions {}

const customRender = (ui: React.ReactElement, options?: CustomRenderOptions): RenderResult =>
  render(ui, { wrapper: AllTheProviders, ...options });

// eslint-disable-next-line react-refresh/only-export-components
export * from '@testing-library/react';

// Override render method
export { customRender as render };
