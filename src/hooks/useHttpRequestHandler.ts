import { useState, useEffect } from 'react';
import { AxiosError } from 'axios';
import { fetchOMDbAPI } from '../services/api';

type FetchResult<T> = {
  data: T | null;
  loading: boolean;
  error: AxiosError | null;
};

const useHttpRequestHandler = <T>(url: string): FetchResult<T> => {
  const [data, setData] = useState<T | null>(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState<AxiosError | null>(null);

  useEffect(() => {
    async function fetchData() {
      try {
        const data = await fetchOMDbAPI<T>(url);

        setData(data);
      } catch (error) {
        const errorMessage = error as AxiosError;
        setError(errorMessage);
      } finally {
        setLoading(false);
      }
    }

    fetchData();
  }, [url]);

  return { data, loading, error };
};

export default useHttpRequestHandler;
