import { renderHook } from '@testing-library/react-hooks';
import { AxiosError } from 'axios';
import useHttpRequestHandler from '../useHttpRequestHandler';
import { fetchOMDbAPI } from 'services/api';
import { act } from '../../test-utils';

jest.mock('services/api', () => ({
  fetchOMDbAPI: jest.fn(),
}));

describe('useHttpRequestHandler', () => {
  beforeEach(() => {
    jest.spyOn(console, 'error').mockImplementation();
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  test('should fetch data successfully', async () => {
    const mockData = { Title: 'Mock Movie', Year: '2022' };
    (fetchOMDbAPI as jest.Mock).mockResolvedValueOnce(mockData);

    const { result, waitForNextUpdate } = renderHook(() => useHttpRequestHandler('mock-url'));

    await waitForNextUpdate();

    expect(result.current.data).toEqual(mockData);
    expect(result.current.loading).toBe(false);
    expect(result.current.error).toBe(null);
    expect(fetchOMDbAPI).toHaveBeenCalledWith('mock-url');
  });

  test('should handle API error', async () => {
    const mockError: AxiosError = {
      isAxiosError: true,
      name: 'AxiosError',
      message: 'API request failed',
      toJSON: jest.fn(),
    };

    (fetchOMDbAPI as jest.Mock).mockRejectedValueOnce(mockError);

    const { result, waitForNextUpdate } = renderHook(() => useHttpRequestHandler('mock-url'));

    await waitForNextUpdate();

    expect(result.current.data).toBe(null);
    expect(result.current.loading).toBe(false);
    expect(result.current.error).toEqual(mockError);
    expect(fetchOMDbAPI).toHaveBeenCalledWith('mock-url');
  });

  test('should handle initial loading state', async () => {
    await act(async () => {
      const { result } = renderHook(() => useHttpRequestHandler('mock-url'));

      expect(result.current.data).toBe(null);
      expect(result.current.loading).toBe(true);
    });
  });
});
