import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App.tsx';
import './assets/styles/index.scss';
import { MoviesListProvider } from './context/MoviesListContext.tsx';

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <MoviesListProvider>
      <App />
    </MoviesListProvider>
  </React.StrictMode>,
);
