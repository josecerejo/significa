import { FC, Suspense, lazy, useEffect } from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Home from './pages/home';
import NotFoundHandling from './components/notFoundHandling';
import Header from './features/header';
import LoadingSpinner from './components/loadingSpinner';
import styles from './app.module.scss';
import Favorites from './features/favorites';
import { useMoviesListContext } from './context/MoviesListContext';
import { Movie } from './types';

const MovieDetailPage = lazy(() => import('./pages/movieDetailPage'));

const App: FC = () => {
  const { favoritesMovies, updateFavoritesMoviesByStorage } = useMoviesListContext();

  useEffect(() => {
    const favoritesMoviesStorage = localStorage.getItem('favoriteMovies');

    if (favoritesMoviesStorage && favoritesMovies.length === 0) {
      const favoritesMovies: Movie[] = JSON.parse(favoritesMoviesStorage);

      updateFavoritesMoviesByStorage(favoritesMovies);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <BrowserRouter>
      <Header />
      <main className={styles['home-page']}>
        <Suspense fallback={<LoadingSpinner />}>
          <Routes>
            <Route path="/" element={<Home />}></Route>
            <Route path="/favorites" element={<Favorites />}></Route>
            <Route path="/movie/:id" element={<MovieDetailPage />}></Route>
            <Route path="*" element={<NotFoundHandling message="Page not found" />}></Route>
          </Routes>
        </Suspense>
      </main>
    </BrowserRouter>
  );
};

export default App;
