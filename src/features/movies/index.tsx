import { FC, Fragment, useState } from 'react';
import styles from './movies.module.scss';
import { useMoviesListContext } from '../../context/MoviesListContext';
import MovieItem from '../../components/movieItem';
import InfiniteScroll from 'react-infinite-scroll-component';
import LoadingSpinner from '../../components/loadingSpinner';

const Movies: FC = () => {
  const { movies, movieSearched, hasMoreMovies, fetchMovies } = useMoviesListContext();
  const [page, setPage] = useState(1);

  const getTheNextPage = () => {
    setPage(page + 1);
    fetchMovies(movieSearched, page + 1);
  };

  return (
    <>
      <h1 aria-live="polite" className={styles['title-searched']} data-testid="movies-searched">
        {movieSearched}
      </h1>

      {movies.length > 0 && (
        <InfiniteScroll
          dataLength={movies.length}
          next={getTheNextPage}
          hasMore={hasMoreMovies}
          loader={
            <div className={styles['loading-infinite-scroll']}>
              <LoadingSpinner />
            </div>
          }
          style={{ overflow: 'hidden' }}
        >
          <section className={styles['movie-container']}>
            {movies.map((movie) => (
              <Fragment key={movie.imdbID}>
                <MovieItem movie={movie} />
              </Fragment>
            ))}
          </section>
        </InfiniteScroll>
      )}
    </>
  );
};

export default Movies;
