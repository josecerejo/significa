import { FC, useCallback } from 'react';
import SearchInput from '../searchInput';
import styles from './header.module.scss';
import { useNavigate } from 'react-router-dom';
import Profile from '../profile';

const Header: FC = () => {
  const navigate = useNavigate();

  const updateRouteToHome = useCallback(() => navigate('/'), [navigate]);

  return (
    <header data-testid="header-component">
      <img
        data-testid="header-image"
        src={'./images/logo.svg'}
        alt="logo image"
        className={styles.logo}
        onClick={updateRouteToHome}
      />
      <div className={styles['right-side-menu']} data-testid="header-right-container">
        <SearchInput />
        <Profile />
      </div>
    </header>
  );
};

export default Header;
