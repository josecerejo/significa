import React from 'react';
import Header from '..';
import { render, screen, fireEvent, waitFor } from '../../../test-utils';

const renderComponent = () => render(<Header />);

describe('Header component', () => {
  test('Header should renders correctly', () => {
    renderComponent();

    const headerImage = screen.getByTestId('header-image');
    const headerComponent = screen.getByTestId('header-component');
    const headerRightContainer = screen.getByTestId('header-right-container');

    expect(headerImage).toBeInTheDocument();
    expect(headerComponent).toBeInTheDocument();
    expect(headerRightContainer).toBeInTheDocument();
  });

  test('Clicking on the logo navigates to home', async () => {
    renderComponent();

    const headerImage = screen.getByTestId('header-image');

    fireEvent.click(headerImage);

    await waitFor(() => {
      expect(window.location.pathname).toBe('/');
    });
  });
});
