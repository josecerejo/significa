import { FC, ChangeEvent, useState, KeyboardEvent, useRef } from 'react';
import styles from './searchInput.module.scss';
import { useMoviesListContext } from '../../context/MoviesListContext';
import { useNavigate, useLocation } from 'react-router-dom';

const SearchInput: FC = () => {
  const [search, setSearch] = useState<string>('');
  const inputSearchRef = useRef<HTMLInputElement | null>(null);
  const navigate = useNavigate();
  const location = useLocation();

  const { fetchMovies, movieSearched } = useMoviesListContext();

  const checkIfTheRouteIsDifferent = () => location.pathname !== '/' && navigate('/');
  const handleSearchUpdate = (e: ChangeEvent<HTMLInputElement>) => setSearch(e.target.value);
  const handleSearchByClick = () => {
    if (inputSearchRef.current) {
      if (inputSearchRef.current.value !== movieSearched) fetchMovies(search);
      checkIfTheRouteIsDifferent();
    }
  };
  const handleSearchByKeyUp = (e: KeyboardEvent<HTMLInputElement>) => {
    if (e.key === 'Enter' && e.currentTarget.value !== movieSearched && e.currentTarget.value.length) {
      fetchMovies(e.currentTarget.value);
      checkIfTheRouteIsDifferent();
    }
  };

  return (
    <div className={styles['search-bar']}>
      <input
        type="text"
        ref={inputSearchRef}
        placeholder="Search"
        onChange={handleSearchUpdate}
        onKeyUp={handleSearchByKeyUp}
      />
      <i className={styles['search-icon']} onClick={handleSearchByClick}></i>
    </div>
  );
};

export default SearchInput;
