import React from 'react';
import Favorites from '..';
import { render, screen, waitFor } from '../../../test-utils';

const renderComponent = () => render(<Favorites />);

describe('Favorites component', () => {
  beforeAll(() => {
    Object.defineProperty(window, 'localStorage', {
      value: {
        getItem: jest.fn(),
        setItem: jest.fn(),
        removeItem: jest.fn(),
      },
      writable: true,
    });
  });

  describe('When does not exists any favorites movies', () => {
    test('renders Favorites component with no movies', () => {
      renderComponent();

      const titleElement = screen.getByTestId('favorites-title');
      const noMoviesElement = screen.getByTestId('favorites-no-movies');

      expect(titleElement).toBeInTheDocument();
      expect(titleElement).toHaveTextContent('Favorites movies');
      expect(noMoviesElement).toBeInTheDocument();
      expect(noMoviesElement).toHaveTextContent("Right now we don't have any favorite movie");
    });
  });

  describe('when exists favorite movies', () => {
    test('renders Favorites component with favorite movies', async () => {
      const favoriteMovies = [
        { Title: 'Movie 1', imdbID: '1' },
        { Title: 'Movie 2', imdbID: '2' },
      ];

      (window.localStorage.getItem as jest.Mock).mockReturnValueOnce(JSON.stringify(favoriteMovies));

      renderComponent();

      await waitFor(() => {
        const movie1Element = screen.getByTestId('favorite-Movie 1');
        const movie2Element = screen.getByTestId('favorite-Movie 2');

        expect(movie1Element).toBeInTheDocument();
        expect(movie2Element).toBeInTheDocument();
      });
    });
  });
});
