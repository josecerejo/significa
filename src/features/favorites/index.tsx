import { FC, useEffect } from 'react';
import styles from './favorites.module.scss';
import { useMoviesListContext } from '../../context/MoviesListContext';
import MovieItem from '../../components/movieItem';

const Favorites: FC = () => {
  const { favoritesMovies, updateFavoritesMoviesByStorage } = useMoviesListContext();

  useEffect(() => {
    const favoritesMoviesStorage = localStorage.getItem('favoriteMovies');

    if (favoritesMovies.length === 0 && favoritesMoviesStorage) {
      updateFavoritesMoviesByStorage(JSON.parse(favoritesMoviesStorage));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <h1 data-testid="favorites-title">Favorites movies</h1>
      {favoritesMovies.length === 0 && (
        <span data-testid="favorites-no-movies">Right now we don't have any favorite movie</span>
      )}
      <section className={styles.container}>
        {favoritesMovies.map((favoritesMovie) => (
          <div key={favoritesMovie.imdbID} data-testid={`favorite-${favoritesMovie.Title}`}>
            <MovieItem movie={favoritesMovie} />
          </div>
        ))}
      </section>
    </>
  );
};

export default Favorites;
