import React from 'react';
import { render, screen } from '@testing-library/react';
import MovieDetails from '..';
import { MovieDetail } from '../../../types';
import { useMoviesListContext } from '../../../context/MoviesListContext';

const mockMovieDetail: MovieDetail = {
  Actors: 'Actor 1, Actor 2',
  Awards: 'Best Movie',
  BoxOffice: '$100 million',
  Country: 'USA',
  DVD: 'Release Date',
  Director: 'Test Director',
  Genre: 'Action',
  Language: 'English',
  Metascore: '75',
  Plot: 'Test plot',
  Poster: 'test-poster.jpg',
  Production: 'Test Production',
  Ratings: [
    { Source: 'IMDB', Value: '8.0' },
    { Source: 'Rotten Tomatoes', Value: '90%' },
  ],
  Released: '2022-01-01',
  Response: 'True',
  Runtime: '120 min',
  Title: 'Test Movie',
  Type: 'Movie',
  Website: 'https://example.com',
  Writer: 'Test Writer',
  Year: '2022',
  imdbID: '123456',
  imdbRating: '8.0',
  imdbVotes: '1000',
};

jest.mock('../../../context/MoviesListContext');

const renderComponent = () => render(<MovieDetails movieDetail={mockMovieDetail} />);

describe('MovieDetails component', () => {
  beforeEach(() => {
    (useMoviesListContext as jest.Mock).mockReturnValue({
      updateFavoritesMovies: jest.fn(),
      favoritesMovies: [],
    });
  });

  test('renders MovieDetails component with correct information', async () => {
    renderComponent();

    const movieDetailSection = screen.getByTestId('movie-detail-section');
    const movieImageContainer = screen.getByTestId('movie-image-container');
    const moviePoster = screen.getByTestId('movie-poster');
    const movieTitle = screen.getByTestId('movie-title');
    const movieInformation = screen.getByTestId('movie-information');
    const movieTitleContainer = screen.getByTestId('movie-title-container');
    const movieRating = screen.getByTestId('movie-rating');
    const moviePlot = screen.getByTestId('movie-plot');
    const movieYearRuntime = screen.getByTestId('movie-year-runtime');
    const movieGenre = screen.getByTestId('movie-genre');
    const movieDirector = screen.getByTestId('movie-director');
    const movieActors = screen.getByTestId('movie-actors');
    const movieWriter = screen.getByTestId('movie-writer');
    const movieBoxOffice = screen.getByTestId('movie-box-office');
    const movieAwards = screen.getByTestId('movie-awards');

    expect(movieDetailSection).toBeInTheDocument();
    expect(movieImageContainer).toBeInTheDocument();
    expect(moviePoster).toHaveAttribute('src', 'test-poster.jpg');
    expect(movieTitle).toHaveTextContent('Test Movie');
    expect(movieInformation).toBeInTheDocument();
    expect(movieTitleContainer).toBeInTheDocument();
    expect(movieRating).toHaveTextContent('IMDB rating - 8.0');
    expect(moviePlot).toHaveTextContent('Test plot');
    expect(movieYearRuntime).toHaveTextContent('2022 - 120 min');
    expect(movieGenre).toHaveTextContent('Action');
    expect(movieDirector).toHaveTextContent('Director - Test Director');
    expect(movieActors).toHaveTextContent('Actors - Actor 1, Actor 2');
    expect(movieWriter).toHaveTextContent('Writer - Test Writer');
    expect(movieBoxOffice).toHaveTextContent('Box office - $100 million');
    expect(movieAwards).toHaveTextContent('Awards - Best Movie');
  });
});
