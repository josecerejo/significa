import { FC } from 'react';
import styles from './movieDetail.module.scss';
import { MovieDetail } from '../../types';
import FavoriteIcon from '../../components/favoriteIcon';

type MovieDetailProps = {
  movieDetail: MovieDetail;
};

const MovieDetails: FC<MovieDetailProps> = ({ movieDetail }) => {
  return (
    <section className={styles['movie-detail']} data-testid="movie-detail-section">
      <div className={styles['movie-image-container']} data-testid="movie-image-container">
        <img src={movieDetail.Poster} alt="image poster" data-testid="movie-poster" />
        <span data-testid="movie-title">{movieDetail.Title}</span>
      </div>
      <div className={styles['movie-information']} data-testid="movie-information">
        <div className={styles['movie-title']} data-testid="movie-title-container">
          <span className={styles['movie-rating']} data-testid="movie-rating">
            IMDB rating - {movieDetail.imdbRating}
          </span>
          <FavoriteIcon movie={movieDetail} />
        </div>
        <span data-testid="movie-plot">{movieDetail.Plot}</span>
        <span data-testid="movie-year-runtime">
          {movieDetail.Year} - {movieDetail.Runtime}
        </span>
        <span data-testid="movie-genre">{movieDetail.Genre}</span>
        <span data-testid="movie-director">Director - {movieDetail.Director}</span>
        <span data-testid="movie-actors">Actors - {movieDetail.Actors}</span>
        <span data-testid="movie-writer">Writer - {movieDetail.Writer}</span>
        <span data-testid="movie-box-office">Box office - {movieDetail.BoxOffice}</span>
        <span data-testid="movie-awards">Awards - {movieDetail.Awards}</span>
      </div>
    </section>
  );
};

export default MovieDetails;
