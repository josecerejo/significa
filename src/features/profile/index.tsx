import { FC, useEffect, useRef, useState } from 'react';
import styles from './profile.module.scss';
import DropdownListItem from '../../components/dropdowListItem';

const Profile: FC = () => {
  const [menuOpened, setMenuOpened] = useState<boolean>(false);
  const myRef = useRef<HTMLDivElement | null>(null);

  const handleClickOutside = (e: MouseEvent) => {
    if (myRef.current && !myRef.current.contains(e.target as Node)) {
      setMenuOpened(false);
    }
  };

  const updateMenuOpened = () => setMenuOpened(!menuOpened);

  useEffect(() => {
    document.addEventListener('mousedown', handleClickOutside);
  }, []);

  return (
    <div ref={myRef}>
      <img
        src="https://cdn0.iconfinder.com/data/icons/avatars-3/512/avatar_hipster_guy-512.png"
        alt="profile avatar"
        className={styles['profile-image']}
        onClick={updateMenuOpened}
      />

      {menuOpened && (
        <ul className={styles['profile-menu-list']}>
          <DropdownListItem title="Favorites" hrefLink={'/favorites'} />
        </ul>
      )}
    </div>
  );
};

export default Profile;
